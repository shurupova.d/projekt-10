## Создание SQL-запросов
### Customers

**1. Вывести поля контактное лицо, адрес и город из таблицы "Customers":**

![ ](https://sun9-79.userapi.com/impg/5G1NdMuaAG-sm1vOYrN0UMA-4Bd9J_aXvBMNtg/k04XqvB1Faw.jpg?size=1280x720&quality=95&sign=eb52b6c2e66e85bb535e77a41aa58796&type=album)

**2. Задать имя (псевдоним) для поля ContactName - Контактное лицо, для Address - Адрес, для City - Город из таблицы Customers:**

![ ](https://sun9-78.userapi.com/impg/iRa5cxlixPvsvduSMqu_XJnBdMbl_DVJW7zRZA/o_phjL1G60U.jpg?size=1280x720&quality=95&sign=a46d29ecb4c19b45f0ffa0bb7f113c4a&type=album)

**3. Вывести данные контактное лицо, адрес и город, где почтовый индекс принадлежит интервалу от 1000 до 2000:**

![ ](https://sun9-23.userapi.com/impg/yy4wf_CHEqX3b9nlUY4r1A45_N7yvKQ9hjtUtw/iykbzYbXsH4.jpg?size=1280x720&quality=95&sign=ee8612a74c5042d017df466ae9243621&type=album)

### Categories
**1. Выбрать те строки, где наименование категории начинается с буквы «С»:**

![ ](https://sun9-73.userapi.com/impg/5I1WTliglXCsWpI9Zqy3abwAO8ruyIMWwKZiiA/tX4F3I2KoLg.jpg?size=1280x720&quality=95&sign=91422caf6888f3404b467929df1ed788&type=album)

**2. Вывести название категории и описание, полученную информацию отсортировать по названию категории в обратном алфавитном порядке:**

![ ](https://sun9-50.userapi.com/impg/YNqLsFcEXOrBr4GALitQJtl71BDrRjf0A6HjPQ/A5lucmA6x_8.jpg?size=1280x720&quality=95&sign=67afeb9d63ef30c1234699f80b4973ff&type=album)

**3. Вывести название категорий, в которых есть слово "sweet" и показать их описание:**

![ ](https://sun9-15.userapi.com/impg/BfK4NVuM8_fvgOUbo1PYFbSYeha9NYsSWZQPNA/7hd984xYuNk.jpg?size=1280x720&quality=95&sign=6103ffd86d2369c67c3382e0718e258f&type=album)

### Employees
**1. Вывести фамилии и имена сотрудников, чьи даты рождения соответствует значениям 1952-02-19, 1955-03-04, 1958-09-19 с указанием примечаний:**

![ ](<https://sun9-51.userapi.com/impg/glF5xYrto0kib2hOoAo5b2GrijNdNcNmMhhaiw/TLs2MlxKbyk.jpg?size=1280x720&quality=95&sign=5c1071a51ba998c1470059616ff0b9da&type=album>)


**2. Вывести поля фамилия и имя с указанием примечаний:**

![ ](https://sun9-35.userapi.com/impg/jrs_-d3rVetsJhMIvCWLiQICHPjhR29UpXIWJg/jNT7tcQv_sk.jpg?size=1280x720&quality=95&sign=32b558334ca780ea96aa607757e32419&type=album)

**3. Показать фамилию человека, чье имя - "Janet":**

![ ](https://sun9-73.userapi.com/impg/IHOSrIQcFAKu2wbINAZlZsENPZtNwz58HDbfqw/SHZJFKtafn8.jpg?size=1280x720&quality=95&sign=431ff470ab4f7ebf3baa4aba73359b6d&type=album)

### OrderDetails
**1. Вывести порядковый номер, номер заказа и номер продукта, отсортировать в алфавитном порядке по номеру продукта:**

![ ](https://sun9-76.userapi.com/impg/m1WgGJajxhQ1PFFxjR-37HFmIFqKBtwgnuW8ag/b3-VnsCtFiI.jpg?size=1280x720&quality=95&sign=2a301403e6480e73b87a79d5673be14a&type=album)


**2. Вывести количество всех заказанных товаров:**

![ ](https://sun9-29.userapi.com/impg/gWFh8wL9yvbt7GN2GQJwmIeXaLGTgPs8NRGvDw/RQ_Q-mrYHlM.jpg?size=1280x720&quality=95&sign=5395deed4af8f8201b5594fecb252a24&type=album)

**3. Показать данные продукта под номер 77, заказанных не менее 10 раз и сгруппированных по номеру заказа:**

![ ](https://sun9-16.userapi.com/impg/46cjQsqgQdQQAWGg6uHMaOXOqii4dhiUZyPNBw/2_-tc3KXbCg.jpg?size=1280x720&quality=95&sign=88c8237f848561b9813971f3f044b00a&type=album)

### Orders
**1. Задать имя (псевдоним) для поля "EmployeeID" - "ID сотрудника", для "CustomerID" - "Пользовательский ID", для "OrderID" - "Номер заказа", для "OrderDate" - "Дата заказа", для "ShipperID" - "ID отправителя" из таблицы "Orders":**

![ ](https://sun9-44.userapi.com/impg/aTXXPMSBgztbD8UljDlYzoCW0Bdl-zJ9gTFjQA/7JmbIGaDFC4.jpg?size=1280x720&quality=95&sign=77a0fbfab0e5f37c980e0dc997167abd&type=album)

**2. Отобрать различные (уникальные) элементы столбца номера заказов из таблицы "Orders":**

![ ](https://sun9-53.userapi.com/impg/fyQLcSBr0v-o0TIbdSBwadgkwhbaXy3RrHsNyQ/DqPYXyoeY1A.jpg?size=1280x720&quality=95&sign=9ece5d9d76f436dded1c9bbb66fda154&type=album)

**3. Показать все заказы, сделанные в период с 1996-08-01 по 1996-08-30:**

![ ](https://sun9-59.userapi.com/impg/zaOLXkmeEAU2BwpkE3-rrtN92Tiy558GC08tCw/CeSIyQi6d0M.jpg?size=1280x720&quality=95&sign=898c15ffa03753428ab4747a445e6a24&type=album)

### Products
**1. В конце года цену всех продуктов на складе пересчитывают – снижают ее на 30%. Написать SQL запрос, который из таблицы "Products" выбирает номер продукта, наименование товара, ID категории и вычисляет новые цены продуктов. Столбец с новой ценой назвать "new_price", цену округлить до 2-х знаков после запятой:**

![ ](https://sun9-53.userapi.com/impg/e8YWf7mTTqE9JZuB7qItNlaLP1X1YSsnoDScIQ/Aco67VHz69E.jpg?size=1280x720&quality=95&sign=848dcc8d246e686eba56ed9eec81222f&type=album)

**2. Вывести наименование товара, идентификатор поставщика и единицу тех продуктов, цена которых меньше 10:**

![ ](https://sun9-57.userapi.com/impg/SbucqSQS3Ly7tkvKH36c4FleO9gUiqLOdBeTRQ/HDcyVhst6K4.jpg?size=1280x720&quality=95&sign=a92ade9e1c6f8d60527a05f57981db72&type=album)

**3. Вывести номер продукта, наименование товара, ID категории и цену продуктов, которая больше или равна 40 и отсортировать в обратном порядке по номеру продукта:**

![ ](https://sun9-30.userapi.com/impg/H_X4_aiArSHP06GEjtn8XUbG-AnI_499KVWvJQ/eIK505w4jJI.jpg?size=1280x720&quality=95&sign=0da5289249339ea8e8d50045a2ea05c4&type=album)

### Shippers
**1. Сделать сортировкупо ID отправителя в обратном порядке:**

![ ](https://sun9-9.userapi.com/impg/FsBxfqTzpOi2aIJeOQthELk7npJD7CAzaElSvg/v9-SQBtQ91s.jpg?size=1280x720&quality=95&sign=634e9346a8a6f5296c41c2a9ba12a9ab&type=album)

**2. Вывести имена отправителей и телефоны, в имени которых содержится буква «S»:**

![ ](https://sun9-6.userapi.com/impg/dSDEAEgvflAZWjRGPfK4A_CsCftXdxCTZDtsxw/YY5jKcFg4F8.jpg?size=1280x720&quality=95&sign=39c857040fc75482da87270b679bd6f7&type=album)

**3. Показать все строки, где ID отправителя больше 1:**

![ ](https://sun9-69.userapi.com/impg/XdkPZDiY2p1Un7WlgAomm6Yfcbn5Ws6rIOg0Sg/Ub4P_O2Id-E.jpg?size=1280x720&quality=95&sign=16e23af96f19bfd0330c35f54471bbc5&type=album)

### Suppliers
**1. Вывести наименование поставщиков из USA:**

![ ](https://sun9-76.userapi.com/impg/0JyZkhh409ORxEwUw-_ei4E5_cwmjT5wUIUrgg/hqM6VmOG6MQ.jpg?size=1280x720&quality=95&sign=28ab19177ab60d9d9ee8fc5dca31e390&type=album)

**2. Вывести все города поставщиков из таблицы "Suppliers":**

![ ](https://sun9-13.userapi.com/impg/GPcwgDd42uiCUVGx-Oa4IlQUIwEZTmSYeP5k7g/WpOYSCtitH4.jpg?size=1280x720&quality=95&sign=9fb924060c172795fd025541c4069202&type=album)

**3. Показать наименование поставщика, контактное лицо, страну, город, адрес из Германии и Франции, наименование поставщика отсортировать по алфавиту:**

![ ](https://sun9-65.userapi.com/impg/n-EVaFCSvzVLR0BD5w4_JFOaEuUpVczTbz32OA/ZP7uFVTMY5I.jpg?size=1280x720&quality=95&sign=4200cd706f295cdfc3e7be9f5597f477&type=album)
