### Свойства веб-хранилищ

| Поведение объектов | Local Storage | Session Storage |
| ------ | ------ | ------ |
| Обновление страницы | + | + |
| Дублирование вкладки браузера | + | + |
| Восстановление страницы | + | + |
| Закрытие и открытии браузера (перезапуска браузера) | + | - |
